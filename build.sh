#!/bin/sh

rm -f public/available.yaml
find src -mindepth 1 -maxdepth 1 -type d | while read LINE ; do
  GAME="$(basename $LINE)"
  echo "Building ${GAME}"
  echo "- name: ${GAME}" >> public/available.yaml
  echo "  version: $(cat $LINE/game.yaml | yq .version)" >> public/available.yaml
  tar -C src -czf "$(realpath public)/${GAME}.tgz" "${GAME}"
done